package com.lukasz;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableCircuitBreaker
@EnableHystrixDashboard
@SpringBootApplication
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}
}

@RestController
class  ServiceRestController{
	 private final BusinessService businessService;

	@Autowired
	public ServiceRestController(BusinessService service) {
		this.businessService = service;
	}

	@GetMapping("/boom")
	public int boom() throws Exception{
		return this.businessService.driveNumber();
	}

}

@Service
class BusinessService {

	public int fallback() {
		return 2;
	}

	@HystrixCommand(fallbackMethod = "fallback")
	public int driveNumber() throws Exception{
		if(Math.random() >  .5) {
			Thread.sleep(1000*3);
			throw new  RuntimeException("Boom! ");
		}
		return 1;
	}
}
